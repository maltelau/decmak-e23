
data {
  int<lower=0> N;
  array[N] int y;
  int N_subj;
  array[N] int subj;
}


parameters {
  real<lower=0,upper=1> alpha;
  vector[N_subj] real alpha_subj;
  real<lower=0,upper=1> theta_initial;
}

transformed parameters{
  array[N] real theta;
  theta[1] = theta_initial;
  for (t in 2:N) {
    theta[t] = theta[t-1] ^ (1/(1+alpha));
  }
  
}

model {
  alpha         ~ beta(1,1);
  theta_initial ~ beta(1,1);
  y ~ bernoulli(theta);
}
// 
// generated quantities {
//   vectirN] int y;
//   y = bernoulli_rng(theta);
// 
// }

