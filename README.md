# decmak-e23

Solutions and attempts and cognitive models for the "Decicision Making" class of autumn 2023 at Århus University

by Malte Lau Petersen

# Parameter recovery for a simple learning model on the chicken sexing problem
![alpha](learn1_alpha.png)
![theta_initial](learn1_thetainitial.png)
