library(cmdstanr)
library(tidyverse)
library(bayesplot)
library(future)
library(furrr)
library(ggdist)
library(ggridges)

ncores <- 1 # how many cores to use to run in parallel
plan(multisession, workers = 16)
nsim <- 100
iter <- 2000 # how many iterations per model

pvl_params <- c("a", "A", "w", "theta")
pvl <- cmdstan_model("module3/pvl.stan")

source("module3/PVL.R")

z <- function(x) {
  return((x - mean(x)) / sd(x))
}

MPD <- function(x) {
  ## maximum posterior density
  if (!all(complete.cases(x))) {
    return(NA)
  } else {
    d <- density(x)
    return(d$x[which.max(d$y)])
  }
}


sim_data <- function(params, seed = NULL) {
  if (!is.null(seed)) {
    set.seed(seed)
  }
  payoff <- z(IGT())

  return(PVL(payoff, 100, params$w, params$A, params$a, params$theta))
}

fit_pvl <- function(.data, .model) {
  return(.model$sample(
    data = list(N = length(.data$x), k = 4, choice = .data$x, reward = .data$X),
    chains = ncores, parallel_chains = ncores,
    iter_warmup = iter / 2, iter_sampling = iter / 2
  ))
}

# sim_data(list(w = 2, A = .5, theta = 3, a = .1))

pvl_params <- expand_grid(
  w = seq(0, 2, 0.5),
  A = seq(0, 2, 0.5),
  theta = seq(0, 5, 1),
  a = seq(0, 1, 0.25)
) %>%
  mutate(.sim = 1:n()) %>%
  nest(true = !.sim) %>%
  mutate(data = map(true, sim_data))

start_time <- Sys.time()
pvl_sim_fits <- mutate(pvl_params, fit = future_map(data, fit_pvl, pvl, .progress = TRUE))
print(Sys.time() - start_time)

datares <- pvl_sim_fits %>%
  mutate(infer = map(fit, function(.m) {
    .m$draws(variables = pvl_params, format = "df") %>%
      reframe(across(!starts_with("."), MPD))
    ## .m$summary(variables = pvl_params, "median") %>%
    ##   pivot_wider(names_from = "variable", values_from = "median")
  })) %>%
  unnest(c(true, infer), names_sep = "_") %>%
  pivot_longer(c(starts_with("true"), starts_with("infer")), names_to = c("source", "parameter"), names_sep = "_") %>%
  pivot_wider(names_from = "source", values_from = "value") #

lims <- group_by(datares, parameter) %>%
  reframe(x = range(true), y = range(true))

datares %>%
  ggplot() +
  geom_boxplot(aes(true, infer, group = true), color = "red") +
  geom_line(aes(x, y), data = lims, linetype = "dashed") +
  geom_smooth(aes(true, infer), method = "lm") +
  facet_wrap(~parameter, scales = "free")
