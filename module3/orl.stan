data {
  int N; // number of trials
  int k; // number of decks, often 4 for IGT
}

parameters {
  // learning rates
  real<lower=0,upper=1> a_rew; 
  real<lower=0,upper=1> a_pun;
  // perseverence decay
  real<lower=0,upper=1> K;
  // softmax temperature
  real<lower=0> theta;
  // weighting different parts of the model
  real omega_f; 
  real omega_p;
}

transformed parameters {
  matrix[N,k] Ev;
  matrix[N,k] Ef;
  matrix[N,k] PS;
  matrix[N,k] p;
  
  // initial values at t=1
  for (deck in 1:k) {
    Ev[1,k] = 0;
    Ef[1,k] = 0;
    PS[1,k] = 1;
    p[1, k] = 0.25;
  }
  
  for (t in 2:N) {
    for (deck in 1:k) {
      Ev[t,deck] = Ev[t-1,deck];
    }
    Ev[t,choice[t]] = Ev[t-1, choice[t]] + a_
  }
}
  
model {

  a_rew   ~ uniform(0,1);
  a_pun   ~ uniform(0,1);
  K       ~ normal(0,10);
  theta   ~ normal(0,10);
  omega_f ~ normal(0,10);
  omega_p ~ normal(0,10);

  #------ set t1 model variables --------------------------
    
  for (t in 2:ntrials) {
    
    #this is imortant mention this as constructing model
    signX[t] <- ifelse(X[t-1]<0,-1,1)
    
    for (d in 1:4) {
      
      # -------- Updating expected values ------------------------
      Ev_update[t,d] <- ifelse(X[t-1]>=0,
                                Ev[t-1,d] + a_rew*((X[t-1]) - Ev[t-1,d]), 
                                Ev[t-1,d] + a_pun*((X[t-1]) - Ev[t-1,d])
      )
                            
      Ev[t,d] <- ifelse(d==x[t-1],Ev_update[t,d],Ev[t-1,d])
      
      # -------- Updating expected frequencies ------------------------
      #update expected frequencies for ALL decks - AS IF THERE WERE ALL CHOSEN
      Ef_cho[t,d] <- ifelse(X[t-1]>=0, 
                              Ef[t-1,d] + a_rew*(signX[t] - Ef[t-1,d]),
                              Ef[t-1,d] + a_pun*(signX[t] - Ef[t-1,d])
      )
      
      #update expected frequencies for ALL decks - AS IF THEY WERE ALL UNCHOSEN. 
      Ef_not[t,d] <- ifelse(X[t-1]>=0, 
                              Ef[t-1,d] + a_pun*(-(signX[t]/3) - Ef[t-1,d]),
                              Ef[t-1,d] + a_rew*(-(signX[t]/3) - Ef[t-1,d])
      ) 
      
      #copy appropriate values to ef variable
      Ef[t,d] <- ifelse(d==x[t-1],Ef_cho[t,d],Ef_not[t,d])  
      
      #-----------Perseverance----------------------------------
      #ifelse needed to disctiminate chosen and unchosen decks
      PS[t,d] <- ifelse(x[t-1]==d,1/(1+K),PS[t-1,d]/(1+K))
      
      #-----------Valence model------------------------------
      V[t,d] <- Ev[t,d] + Ef[t,d]*omega_f + PS[t,d]*omega_p
      
      #----------softmax part 1-------------
      exp_p[t,d] <- exp(theta*V[t,d])
      
    }
    
    #----------softmax part 2-------------
    for (d in 1:4) {
      p[t,d] <- exp_p[t,d]/sum(exp_p[t,])
    }
      
    x[t] ~ dcat(p[t,])
    
  }
}