data {
  int N; // number of trials
  int k; // number of decks, often 4 for IGT
  // choice and reward on trial t
  array[N] int choice;
  array[N] real reward;
}

parameters {
  // learning rate
  real<lower=0,upper=1> a; 
  // shape of utility function
  real<lower=0> A;
  // loss aversion
  real<lower=0> w;
  // softmax temperature
  real<lower=0> theta;
}

transformed parameters {
  matrix[k,N] Ev; // Ev for each trial, deck combo
  matrix[k,N] p;
  vector[k] v; // updated Ev for each deck
  real prediction_error;

  v = rep_vector(0, k);
  p[,1] = rep_vector(0.25, k);

  for (t in 2:N) {

    if (reward[t-1] > 0) {
      prediction_error = pow(reward[t-1], A) - v[choice[t-1]]; // prediction error
    } else {
      prediction_error = -w * pow(abs(reward[t-1]), A) - v[choice[t-1]];
    }
    v[choice[t-1]] = v[choice[t-1]] + a * prediction_error; // value update
    Ev[,t] = v;
    p[,t] = softmax(theta * Ev[,t]);
  }
}


model {

  // priors. stan uses mean,sd as parameters where jags uses mean, precision
  // so normal(0,10) in stan is the same as dnorm(0,0.1) in jags.
  w ~ normal(1, 10);
  A ~ normal(1, 10);
  theta ~ normal(1,10);
  a ~ uniform(0,1);

  for (t in 2:N) {
    choice[t] ~ categorical(p[,t]);
  }
}
