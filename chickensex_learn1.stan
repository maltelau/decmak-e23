
data {
  int<lower=0> N;
  array[N] int y;
}

parameters {
  real<lower=-1,upper=1> alpha;
  real<lower=0,upper=1> theta_initial;
}

transformed parameters{
  array[N] real<lower=0,upper=1> theta;
  theta[1] = theta_initial;
  for (t in 2:N) {
    theta[t] = theta[t-1] ^ (1/(1+alpha));
  }
  
}

model {
  alpha         ~ normal(0,2);
  theta_initial ~ normal(0.5, 1);
  y ~ bernoulli(theta);
}
// 
// generated quantities {
//   vectirN] int y;
//   y = bernoulli_rng(theta);
// 
// }

