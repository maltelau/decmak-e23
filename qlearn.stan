
data {
  int N; // number of trials
  int K; // number of categories
  array[N] int choice; // categorical choices
  array[N] real r; // rewards
}

parameters {
  // real<lower=0> beta;
  // real lambda;
  real<lower=0> alpha;
  
}


model {
  matrix[K, N] VA;
  //matrix[K, N] delta_VA;
  matrix[K, N] theta;

  // beta ~

  
  for (k in 1:K) {
    // delta_VA[k,1] = 0;
    VA[k,1] = 0;
  }
  
  for (t in 2:N) {
    //for (k in 1:K) {
      //delta_VA[k,t] = alpha[k] * (lambda - VA[k,t-1]);
      //VA[k,t] = VA[k,t-1] + alpha * (r[t] - VA[k,t-1]);
    //}
    VA[choice[t],t] = VA[choice[t],t-1] + alpha * (r[t] - VA[choice[t],t-1]);
    theta[,t] = softmax(VA[,t]);
    choice[t] ~ categorical(theta[,t]);
  }
}
