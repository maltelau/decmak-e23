data {
  int N; // number of trials (total for all subjects)
  int C; // number of decks, often 4 for IGT
  int Nsubj; // how many subjects
  // choice and reward
  array[N] int trial;
  array[N] int subject;
  array[N] int choice;
  array[N] real reward;
  array[N] real reward_sign;

  int N_beta;
  matrix[N, N_beta] X;
}

parameters {
  // learning rates
  // hierarchical
  vector[N_beta] a_rew_betas;
  real<lower=0> a_rew_sd;
  vector[Nsubj] a_rew_subj;

  vector<lower=0,upper=1>[Nsubj] a_pun_subj;
  vector<lower=0>[Nsubj] K_subj;
  vector<lower=0>[Nsubj] theta_subj;
  vector[Nsubj] omega_f_subj;
  vector[Nsubj] omega_p_subj;

  // TODO: not hierarchical YET
  //real<lower=0,upper=1> a_rew;
  //real<lower=0,upper=1> a_pun;
  // perseverence decay
  //real<lower=0> K;
  // softmax temperature
  //real<lower=0> theta;
  // weighting different parts of the model
  // real omega_f;
  // real omega_p;
}

transformed parameters {
  matrix[C,N] Ev; // Value
  matrix[C,N] Ef; // Frequency
  matrix[C,N] PS; // Perseverence
  matrix[C,N] p;  // probability of choice
  vector<lower=0,upper=1>[N] a_rew;
  vector<lower=0,upper=1>[Nsubj] a_pun;
  vector<lower=0>[Nsubj] K;
  vector<lower=0>[Nsubj] theta;
  vector[Nsubj] omega_f;
  vector[Nsubj] omega_p;

  // a_rew = a_rew_subj;
  a_pun = a_pun_subj;
  K = K_subj;
  theta = theta_subj;
  omega_f = omega_f_subj;
  omega_p = omega_p_subj;
  a_rew = inv_logit(X * a_rew_betas + a_rew_subj[subject]);


  for (i in 1:N) {
    if (trial[i] == 1) {
      // initial values at trial 1
      for (deck in 1:C) {
        Ev[deck,i] = 0;
        Ef[deck,i] = 0;
        PS[deck,i] = 1;
        p [deck,i] = 0.25;
      }
    } else {
      for (deck in 1:C) {
        if (deck == choice[i-1]) {
          // chosen deck
          PS[deck, i] = 1.0 / (1 + K[subject[i]]);
          if (reward[i-1] >= 0) {
            // positive, reward
            Ev[deck, i] = Ev[deck, i-1] + (a_rew[i] * (reward[i-1] - Ev[deck, i-1]));
            Ef[deck, i] = Ef[deck, i-1] + (a_rew[i] * (reward_sign[i-1]) - Ef[deck, i-1]);
          } else {
            // negative, loss
            Ev[deck, i] = Ev[deck, i-1] + (a_pun[subject[i]] * (reward[i-1] - Ev[deck, i-1]));
            Ef[deck, i] = Ef[deck, i-1] + (a_pun[subject[i]] * (reward_sign[i-1]) - Ef[deck, i-1]);
          }
        } else {
          // the other, unchosen decks
          Ev[deck, i] = Ev[deck, i-1];
          PS[deck, i] = PS[deck, i-1] / (1 + K[subject[i]]);
          if (reward[i-1] >= 0) {
            // positive, reward
            Ef[deck, i] = Ef[deck, i-1] + (a_rew[i] * ((-reward_sign[i-1])/(C-1)) - Ef[deck, i-1]);
          } else {
            // negative, loss
            Ef[deck, i] = Ef[deck, i-1] + (a_pun[subject[i]] * ((-reward_sign[i-1])/(C-1)) - Ef[deck, i-1]);
          }
        }
      }
      p[,i] = softmax(theta[subject[i]] * (Ev[,i] + Ef[,i]*omega_f[subject[i]] + PS[,i]*omega_p[subject[i]]));
    }
  }
}



model {

  // a_rew_subj   ~ uniform(0,1);
  a_rew_betas ~ normal(0, 10);
  a_rew_sd ~ normal(0, 10);
  a_rew_subj ~ normal(0, a_rew_sd);

  a_pun_subj   ~ uniform(0,1);
  K_subj       ~ normal(0,10);
  theta_subj   ~ normal(0,10);
  omega_f_subj ~ normal(0,10);
  omega_p_subj ~ normal(0,10);

  for (t in 2:N) {
    choice[t] ~ categorical(p[,t]);
  }
}
