
data {
  int<lower=0> N;
  real alpha;
  real theta_initial;
}

transformed data {
  vector[N] theta;
  theta[1] = theta_initial;
  for (t in 2:N) {
    theta[t] = theta[t-1] ^ (1/(1+alpha));
  }
}

generated quantities {
  array[N] int y;
  y = bernoulli_rng(theta);

}

